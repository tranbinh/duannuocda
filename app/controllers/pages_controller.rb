class PagesController < ApplicationController
  def about
  end

  def home
  end

  def contact
  end

  def product
  end

  def news
  end
end
